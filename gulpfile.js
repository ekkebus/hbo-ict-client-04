const {series, parallel, watch} = require("gulp");
const {src, dest} = require("gulp");
const browserSync = require("browser-sync").create();
const concat = require("gulp-concat");
//import gulp-sass
const sass = require('gulp-sass');  

const html = function(){
    return src("./src/*.html")
    .pipe(dest("./dist"))
}

//aangepaste css functie, die maar één scss bestand laadt
const scss = function(){
    return src("./src/sass/style.scss")
    .pipe(sass().on('error', sass.logError))   //compile sass
    .pipe(dest("./dist"));
}

const js = function(){
    return src("./src/js/**/*.js")
    .pipe(concat("app.js"))
    .pipe(dest("./dist"));
}

const watchFiles = () => {
    browserSync.init({server:{baseDir: "./dist/"}});
    watch(["./src/*.html"], series(html));
    watch(["./src/js/**/*.js"], series(js));
    //aangepaste selectie
    watch(["./src/sass/**/*.scss"], series(scss));

    watch("./dist/*.html").on("change", browserSync.reload);
    watch("./dist/**/*.js").on("change", browserSync.reload);
    watch("./dist/**/*.css").on("change", browserSync.reload);
}

exports.build = parallel(html, scss, js);
exports.watch = watchFiles;