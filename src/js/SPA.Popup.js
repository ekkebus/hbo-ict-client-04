SPA.Popup = (function ($) {
    let _root;
    let _popup;

    function _init(root) {
        _root = root;
    }

    function _show(bericht) {
        if (_popup) {
            remove();
        }

        _popup = $("<div>" + bericht + "</div>");
        let knop = $("<input type='button' value='X'/>");
        $(knop).click(remove);

        $(_popup).append(knop);
        $(_root).append(_popup);
    }

    function remove() {
        $(_popup).remove();
        _popup = undefined;
    }
    
    return {
        init: _init,
        show: _show
    }
})($);