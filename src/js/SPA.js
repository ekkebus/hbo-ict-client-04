const SPA = (function ($) {

    function _init(root) {
        SPA.Popup.init(root);
        let button = $("<input type='button' value='dont click me :)'>");
        $(button).click(_showWeather);
        $(root).append(button);
        return true;
    }

    function _showWeather() {
        return new Promise(function (resolve, reject) {
            var elburg = SPA.Weer.getWeather("Elburg");
            var zwolle = SPA.Weer.getWeather("Zwolle");

            Promise.all([elburg, zwolle])
                .then(function (values) {
                    var bericht = '';
                    $(values).each(function (index, value) {
                        bericht += value.main.temp + " ";
                    })
                    SPA.Popup.show(bericht);
                    resolve();
                }).
                catch(function (data) {
                    SPA.Popup.show(data);
                    reject();
                });
        })
    }

    return {
        init: _init,
        showWeather: _showWeather
    }
})($);

