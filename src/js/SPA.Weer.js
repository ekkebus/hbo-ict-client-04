SPA.Weer = (function ($) {
    var _configMap = {
        endPoint: 'http://api.openweathermap.org/data/2.5/weather',
        apikey: '38c54cfa93fde5b1a7a55e4c7f6943e0'
    };

    function _getWeather(locatie) {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: _configMap.endPoint + '?q=' + locatie + '&APPID=' + _configMap.apikey,
                success: function (data) {
                    resolve(data);
                },
                error: function (data) {
                    reject("Kan de data niet ophalen");
                }
            })
        });
    }

    return {
        getWeather: _getWeather
    }
})($);